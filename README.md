# README #

Useful R code that can be shared freely

### What is this repository for? ###

* snippets of R code that are highly useful and free to be shared
* May be created by outsideaubrey or others - I will make every attempt to give others credit if it is their original work

### How do I get set up? ###

* Install R/Rstudio
* May need to install Rtools or individual libraries - see individual code or comments for details

### Contribution guidelines ###

* Anyone is welcome to push suggestions for changes or additions 

### Who do I talk to? ###

* Aubrey Schrader
* outsideaubrey@gmail.com